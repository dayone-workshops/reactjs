import React from 'react'
import ReactDOM from 'react-dom'
import 'styles/normalize.css'
import 'styles/bootstrap.css'
import 'styles/main.css'
import App from './App'

ReactDOM.render(<App />, document.getElementById('main'))

