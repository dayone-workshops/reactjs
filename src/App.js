import React, { useState, useEffect } from 'react'
import TopBar from './top-bar'

const App = () => (
	<div data-testid="main-app">
		<TopBar />

		<div className="container-fluid">
			<h1>Welcome!</h1>
			<div className="row fill-height">
				<div className="col-md-8 pb-3" />
				<div className="col-md-4" />
			</div>
		</div>
	</div>
)

export default App
