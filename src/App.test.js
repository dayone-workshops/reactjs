import React from 'react'
import ReactDOM from 'react-dom'
import { act } from 'react-dom/test-utils'
import App from './App'

import { mount } from 'enzyme'
import { render } from '@testing-library/react'

it('renders without crashing enzyme', () => {
	const app = mount(<App />)
	const mainDiv = app.find('[data-testid="main-app"]')
	expect(mainDiv.exists()).toEqual(true)
})

it('renders without crashing testing lib', () => {
	const app = render(<App />)
	const mainDiv = app.getByTestId('main-app')
})
