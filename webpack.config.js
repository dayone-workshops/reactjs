const webpack = require('webpack')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssPlugin = require('optimize-css-assets-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = env => {
	const PROD = (env && env.production) || process.env.NODE_ENV == 'production'

	const extractSass = new MiniCssExtractPlugin({
		filename: 'styles.[chunkhash].css',
		disable: !PROD,
	})

	const PLUGINS = [
		extractSass,
		new OptimizeCssPlugin(),
		new HtmlWebpackPlugin({ template: 'src/main.html' }),
		new webpack.DefinePlugin({
			__DEV__: !PROD,
			'process.env': {
				NODE_ENV: JSON.stringify(PROD ? 'production' : 'development'),
			},
		}),
	]

	const DEV_PLUGINS = [new BundleAnalyzerPlugin({ openAnalyzer: false })]

	const PROD_PLUGINS = [
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
	]

	const webpackPlugins = PROD ? PLUGINS.concat(PROD_PLUGINS) : PLUGINS.concat(DEV_PLUGINS)

	return {
		mode: PROD ? 'production' : undefined,
		entry: [path.resolve(__dirname, 'src', 'main.jsx')],
		stats: {
			colors: true,
		},
		resolve: {
			modules: [path.resolve(__dirname, 'src'), path.resolve(__dirname, 'node_modules')],
			extensions: ['.mjs', '.js', '.jsx', '.json', '.messages'],
		},
		output: {
			filename: '[name].[chunkhash].js',
			publicPath: '/',
		},
		module: {
			rules: [
				{
					test: /\.(mjs|js|jsx)$/,
					loader: 'babel-loader',
					exclude: [/node_modules/, /\.test\.js?$/],
				},
				{
					test: /\.(css|scss)$/,
					use: [MiniCssExtractPlugin.loader, 'css-loader'],
				},
				{
					test: /\.(eot|otf|woff|woff2|png|jpg|gif|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					loader: 'url-loader?limit=8192',
				},
			],
		},
		plugins: webpackPlugins,
		devServer: {
			contentBase: './src',
			host: '0.0.0.0',
			port: 8080,
			historyApiFallback: {
				index: 'app/index.html',
				rewrites: [
					{
						from: /[^/]+\.(eot|otf|woff|woff2|png|jpg|gif|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
						to: ({ match }) => {
							return `/${match[0]}`
						},
					},
				],
			},
		},
	}
}
