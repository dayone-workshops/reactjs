module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es6: true,
		'jest/globals': true,
	},
	extends: ['eslint:recommended', 'plugin:react/recommended', 'plugin:prettier/recommended'],
	parser: 'babel-eslint',
	parserOptions: {
		sourceType: 'module',
		ecmaVersion: 6,
		ecmaFeatures: {
			experimentalObjectRestSpread: true,
			jsx: true,
		},
	},
	plugins: ['react', 'react-hooks', 'prettier', 'jest'],
	rules: {
		'no-console': 'warn',
		'no-extra-boolean-cast': 'off',
		'react/jsx-uses-vars': 'error',
		'react/jsx-uses-react': 'error',
	},
	settings: {
		react: {
			version: 'detect',
		},
	},
}
